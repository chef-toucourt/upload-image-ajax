const image_displayed = document.body.querySelector('img[id="image_displayed"]');

const upload_image_input = document.body.querySelector('input[id="upload_image_form_image"]');

image_displayed.addEventListener(
    'click',
    triggerInputFile
);

upload_image_input.addEventListener(
    'change',
    submitImageFile
)

function triggerInputFile() {
    upload_image_input.click();
}

function submitImageFile({currentTarget}) {
    if (currentTarget.files.length === 0) {
        return;
    }

    sendAJAXRequest();
}

function sendAJAXRequest() {
    const form_data = new FormData(
        upload_image_input.closest('form')
    );

    const fetch_options = {
        method: 'POST',
        body: form_data,
        headers: {
            "Accept": "application/json",
            "X-Requested-With": "XMLHttpRequest"
        }
    }

    fetch(
        window.location.href,
        fetch_options
    )
        .then(response => response.json())
        .then(data => replaceImageDisplayedIfUploadIsValid(data))
        .catch(error => console.error(error));
}

function replaceImageDisplayedIfUploadIsValid(data) {
    if (data['status'] !== 200) {
        alert(data['error']);

        return;
    }

    /*
        Note: you have to change the alt attribute related to the image too... :-)
     */
    image_displayed.setAttribute(
        'src',
        data['relative_path']
    );
}