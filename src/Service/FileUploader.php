<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @author Chef-Toucourt
 */
final class FileUploader
{
    public function __construct(
        private SluggerInterface $slugger,
        private string $projectDirectory,
        private string $uploadDirectory,
    )
    {}

    public function upload(UploadedFile $file): array
    {
        $originalFilename = pathinfo(
            path: $file->getClientOriginalName(),
            flags: PATHINFO_FILENAME
        );

        $safeFilename = $this
                            ->slugger
                            ->slug(
                                string: $originalFilename
                            )
                            ->lower();

        $fileName = $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();

        $absolutePath = $this->projectDirectory . '/public/' . $this->uploadDirectory;

        try {
            $file->move(
                directory: $absolutePath,
                name: $fileName
            );
        } catch (FileException $error) {
            throw new FileException($error);
        }

        return [
            'absolute_path' => $absolutePath . $fileName,
            'filename'      => $fileName,
            'relative_path' => $this->uploadDirectory . $fileName
        ];
    }
}