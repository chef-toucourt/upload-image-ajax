<?php

namespace App\Controller;

use App\Form\UploadImageFormType;
use App\Service\FileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author Chef-Toucourt
 */
final class UploadImageController extends AbstractController
{
    #[
        Route(
            path: '/',
            name: 'app_home',
            methods: [
                Request::METHOD_GET,
                Request::METHOD_POST
            ]
        )
    ]
    public function index(
        FileUploader $fileUploader,
        Request $request
    ): Response
    {
        $form = $this->createForm(
            type: UploadImageFormType::class
        );

        $form->handleRequest($request);

        if (
            $form->isSubmitted()
            && $form->isValid()
        ) {
            $uploadedFile = $form->get('image')->getData();

            if ($uploadedFile instanceof UploadedFile === false) {
                return $this->json(
                    data: [
                        'error' => 'Transfert invalide',
                        'status'  => 400
                    ],
                    status: 400
                );
            }

            [
                'relative_path' => $relativePath
            ] = $fileUploader->upload($uploadedFile);

            // do some stuff like persist in database...

            return $this->json(
                data: [
                    'relative_path' => $relativePath,
                    'status'        => 200
                ]
            );
        }

        return $this->renderForm(
            view: 'upload_image/index.html.twig',
            parameters: [
                'form' => $form
            ]
        );
    }
}
