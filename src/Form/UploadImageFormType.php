<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

/**
 * @author Chef-Toucourt
 */
final class UploadImageFormType extends AbstractType
{
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void
    {
        $builder
            ->add(
                child: 'image',
                type: FileType::class,
                options: [
                    'attr'        => [
                        'hidden' => true,
                    ],
                    'constraints' => [
                        new Image(),
                    ],
                    'label'       => false,
                    'mapped'      => false,
                    'required'    => true,
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }
}
